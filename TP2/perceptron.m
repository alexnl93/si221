function [w1, cpt] = perceptron(b, w1, n, ima, ima_seuil)

    %Initialisation des variables de l'alg
    cpt = 0; %nombre d'époques
    prec = 0; %b/w est toujours different de zero dans notre

    %Création de la base de test
    
    while(true)
        for i = 1 : 512
            for j = 1 : 512
                %On passe par la fonction seuil
                if (ima(i,j)*w1 > 127)
                    result = 1;
                else
                    result = -1;
                w1 = w1 + n*(ima_seuil(i,j) - result)*ima(i,j);
                end
            end
        end
        if (b/w1 == prec)
            break; %la valeur converge
        else
            cpt = cpt + 1;
            prec = b/w1;
        end
    end