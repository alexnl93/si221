clear;

% On charge l'image de la base de test
ima = ima2mat('BASEIMAGES/landsattarasconC4.ima');
%Pour avoir la méme dimension
ima_seuil = ima;

%Création de la base test
for i = 1 : 512
    for j = 1 : 512
        if (ima(i,j) > 30)
            ima_seuil(i,j) = 1;
        else
            ima_seuil(i,j) = -1;
        end
    end
end

%Pour avoir la méme dimension
ima_seuil_false = ima;

%Création de la base test avec l'erreur: les valeurs à 110 seront dans la classe -1 (la classe2)
for i = 1 : 512
    for j = 1 : 512
        if (ima(i,j) > 30)
            if(ima(i,j) > 140)
                ima_seuil_false(i,j) = -1;
            else
                ima_seuil_false(i,j) = 1;
            end
        else
            ima_seuil_false(i,j) = -1;
        end
     end
end

%Soit cpt le nombre de pixel different dans les deux bases d'apprentissage
cpt = 0;

for i = 1 : 512
        for j = 1 : 512
            if (ima_seuil_false(i,j) ~= ima_seuil(i,j))
                cpt = cpt + 1;
            end
        end
end

disp('Le nombre de pixel qui changent entre les deux bases dapprentissages:');
disp(cpt);

%Aprrentissage avec la mauvaise base de test
b=127;
w1=1;
n = 0.01;			%(valeur du etha)
cpt = 0;			%nombre d'époques
[w1, cpt] = perceptron(b, w1, n, ima, ima_seuil_false);

disp('avec la mauvaise base');
disp('b vaut:');
disp(b);
disp('w1 vaut:');
disp(w1);
disp('b/w1 vaut:');
disp(b/w1);
disp('Le nombre d époques nécessaire est');
disp(cpt);

    
%Soit ima_class_false le vecteur obtenu grâce au mauvais classifieur
ima_class_false = ima; 
for i = 1 : 512
    for j = 1 : 512
        if (ima(i,j) > floor(b/w1))
            ima_class_false(i,j) = 1;
        else
            ima_class_false(i,j) = -1;
        end
     end
end
%Etonnament la valeur seuil b/w ne change pas


%Aprrentissage avec la bonne base de test
b=127;
w1=1;
n = 0.01;			%(valeur du etha)
cpt = 0;			%nombre d'époques
[w1, cpt] = perceptron(b, w1, n, ima, ima_seuil);

disp('avec la bonne base');
disp('b vaut:');
disp(b);
disp('w1 vaut:');
disp(w1);
disp('b/w1 vaut:');
disp(b/w1);
disp('Le nombre d époques nécessaire est');
disp(cpt);

%Soit ima_class le vecteur obtenu grâce au bon classifieur
ima_class = ima; 
for i = 1 : 512
    for j = 1 : 512
        if (ima(i,j) > floor(b/w1))
            ima_class(i,j) = 1;
        else
            ima_class(i,j) = -1;
        end
    end
end

%Soit cpt le nombre de pixel changé par cette erreur
cpt = 0;

for i = 1 : 512
    for j = 1 : 512
        if (ima_class_false(i,j) ~= ima_seuil(i,j))
            cpt = cpt + 1;
        end
    end
end

disp('Le nombre de pixel concerné par cette errreur est:');
disp(cpt);

%Soit erreur, le pourcentage d'erreur de notre classifieur sur la base
%de test
erreur = test_rate(ima_seuil, ima_class_false);
disp('erreur vaut:');
disp(erreur);
%Conclusion il y a 6 pourcents d'eurreur