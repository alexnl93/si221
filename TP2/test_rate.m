%Retourne le pourcentage de pixel différents dans une image
function [r] = test_rate(ima_seuil, ima_class)

nbr = 512*512;
cpt = 0;

for i = 1 : 512
        for j = 1 : 512
            if (ima_class(i,j) ~= ima_seuil(i,j))
                cpt = cpt + 1;
            end
        end
end

r = cpt/nbr*100;