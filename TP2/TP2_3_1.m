clear;

%Mise en place du corpus d'apprentissage

    % On charge l'image de la base de test
    ima = ima2mat('BASEIMAGES/landsattarasconC4.ima');
    %Pour avoir la méme dimension
    ima_seuil = ima;
    
    %Création de la base test
    for i = 1 : 512
        for j = 1 : 512
            if (ima(i,j) > 30)
                ima_seuil(i,j) = 1;
            else
                ima_seuil(i,j) = -1;
            end
        end
    end


%Apprentissage du perceptron - la théore

    %Schéma avec l'orthogonal de W et W
    figure(1);
    axis([0 255 0 8.5]);hold on;
    %Droite de l'orthogonal de W qui sera en rouge sur le graphique
    plot([0,255],[0,8.5],'r');hold on;
    %Droite W qui sera en bleu sur le graphique
    plot([0,8.5],[255,0],'b');hold on;
    %WARNING: le repère est orthogonal non orthonormé
    %WARNING: c'est pour cela que les droites n'ont pas l'air
    %WARNING: perpendiculaires
    
%Apprentissage du perceptron - Implémentation

    %Initialisation des variables de l'alg
    b=127;
    w1=1;
    n = 0.01; %(valeur du etha)
    cpt = 0; %nombre d'époques
    prec = 0; %memorisation de la valeur précédente

    %Apprentissage
    while(true)
        for i = 1 : 512
            for j = 1 : 512
                %On passe par la fonction seuil
                if (ima(i,j)*w1 > 127)
                    result = 1;
                else
                    result = -1;
                w1 = w1 + n*(ima_seuil(i,j) - result)*ima(i,j);
                end
            end
        end
        if (b/w1 == prec)
            break; %la valeur converge
        else
            cpt = cpt + 1;
            prec = b/w1;
        end
    end
        
    disp('b vaut:');
    disp(b);
    disp('w1 vaut:');
    disp(w1);
    disp('b/w1 vaut:');
    disp(b/w1);
    disp('Le nombre d époques nécessaire est');
    disp(cpt);
    
    %Soit ima_class le vecteur obtenu grâce au classifieur
    ima_class = ima; 
       for i = 1 : 512
        for j = 1 : 512
            if (ima(i,j) > floor(b/w1))
                ima_class(i,j) = 1;
            else
                ima_class(i,j) = -1;
            end
        end
       end
    
    %Soit erreur, le pourcentage d'erreur de notre classifieur sur la base
    %de test
    erreur = test_rate(ima_seuil, ima_class);
    disp('erreur vaut:');
    disp(erreur);
    %Conclusion il y a 6 pourcents d'eurreur
    
    %Pour la question sur etha, nous avons implémenté la fonction peceptron
    %qui renvoie w1 trouvé par l'algorythme ainsi que le nombre d'époque
    %qu'il a fallu pour la trouvé
    
    %tab est le tableau qui contient nos trinomes (n, b/w1, cpt)
    tab = [];
    
    for k = 0 : 6 
    
    b=127;
    w1=1;
    n = 0.1/(10^k); %(valeur du etha)
    cpt = 0; %nombre d'époques
    [w1, cpt] = perceptron(b, w1, n, ima, ima_seuil);
    tab = [tab; [n, b/w1, cpt]];
    end
    
    disp(tab);

    %On constate que l'algorithme converge vers 31, soit l'entier supérieur
    %de la valeur désirée. De plus, on remarque que plus etha augmente,
    %plus b/w1 tend vers l'entier de decision 30 plus un. Ce nombre est le
    %nombre que l'on a choisit pour trier les pixels dans la question 1
    
        %Affichons maintenant les deux images:
       for i = 1 : 512
            for j = 1 : 512
                if (ima_class(i,j) == 1)
                    ima_class(i,j) = 255;
                else
                    ima_class(i,j) = 0;
                end
            end
       end
       
        for i = 1 : 512
            for j = 1 : 512
                if (ima_seuil(i,j) == 1)
                    ima_seuil(i,j) = 255;
                else
                    ima_seuil(i,j) = 0;
                end
            end
       end
    
    figure(2);
    image(ima_seuil);
    
    figure(3);
    image(ima_class);
