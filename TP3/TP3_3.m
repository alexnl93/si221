clear;

correspondance;
load('bigramfrancais.mat');
a = load('bigramenglish.mat');
a = a.matrice_trans;

b = load('bigramfrancais.mat');
b = b.matrice_trans;

phrase_en = [' to be or not to be '];
phrase_fr = [' etre ou ne pas etre '];

s = size(phrase_en);
lenght_phrase_en = s(2);

s = size(phrase_fr);
lenght_phrase_fr = s(2);

%vraisemblance de la phrase en anglais avec le dico anglais et français
vraisemblance_en_endico = 1;
vraisemblance_en_frdico = 1;
for i = 1 : (lenght_phrase_en - 1)
    j = 1;
    k = 2;
    while (phrase_en(i) ~= corresp{j,2})
        j = j+1;
    end
    while (phrase_en(i+1) ~= corresp{k,2})
        k = k+1;
    end
    vraisemblance_en_endico = vraisemblance_en_endico * a(j,k);
    vraisemblance_en_frdico = vraisemblance_en_frdico * b(j,k);
    
end

%vraisemblance de la phrase en français avec le dico anglais et français
vraisemblance_fr_endico = 1;
vraisemblance_fr_frdico = 1;
for i = 1 : (lenght_phrase_fr - 1)
    j = 1;
    k = 2;
    while (phrase_fr(i) ~= corresp{j,2})
        j = j+1;
    end
    while (phrase_fr(i+1) ~= corresp{k,2})
        k = k+1;
    end
    vraisemblance_fr_endico = vraisemblance_fr_endico * a(j,k);
    vraisemblance_fr_frdico = vraisemblance_fr_frdico * b(j,k);
    
end

disp('vraisemblance de la phrase en anglais avec le dico anglais');
disp(vraisemblance_en_endico);

disp('vraisemblance de la phrase en anglais avec le dico français');
disp(vraisemblance_en_frdico);

disp('vraisemblance de la phrase en français avec le dico anglais');
disp(vraisemblance_fr_endico);

disp('vraisemblance de la phrase en français avec le dico français');
disp(vraisemblance_fr_frdico);