function [j] = repartition(line)

u = rand;
j = 1;
limite = line(j);
while(u > limite)
    j = j+1;
    limite = limite + line(j);
end
end