clear;

cam1 = ima2mat('BASEIMAGES/landsattarasconC1.ima');
cam2 = ima2mat('BASEIMAGES/landsattarasconC2.ima');
cam3 = ima2mat('BASEIMAGES/landsattarasconC3.ima');

nlig = size(cam1,1);
ncol = size(cam1,2);

cam1 = (cam1(:) - mean(cam1(:))) / std(cam1(:)) ;
cam2 = (cam2(:) - mean(cam2(:))) / std(cam2(:)) ;
cam3 = (cam3(:) - mean(cam3(:))) / std(cam3(:)) ;

X = [cam1 cam2  cam3];

COV = X'*X;

[V,D] = eig(COV);
% Les vecteurs propores associés au vp sont les colonnes de la matrices

% Mise en forme des valeurs propres dans une ligne
D = (D*ones(3,1))';

% Calcul de l'inertie
I = D / sum(D);

%On a landa3 qui vaut 7,6*10^5 qui repond au critère de 95%
%Conclusion: Il suffit de garder une image
