clear;

%Pour le choix des centres equidistribués, on prend un pixel où il y a
%l'eau, et un pixel où il y a la terre.

cam1 = ima2mat('BASEIMAGES/landsattarasconC7.ima');
figure(1);
image(cam1);

%En ayant ouvert l'image, on peut choisir quels sont les coordonées des
%centres des clusters choisit.
%cl:
ligne_terre = 214;
colonne_terre = 55;

ligne_eau = 131;
colonne_eau = 265;


cam1 = ima2mat('BASEIMAGES/landsattarasconC1.ima');
cam2 = ima2mat('BASEIMAGES/landsattarasconC2.ima');
cam3 = ima2mat('BASEIMAGES/landsattarasconC3.ima');

nlig = size(cam1,1);
ncol = size(cam1,2);

cam1 = (cam1 - mean(cam1(:))) / std(cam1(:)) ;
cam2 = (cam2 - mean(cam2(:))) / std(cam2(:)) ;
cam3 = (cam3 - mean(cam3(:))) / std(cam3(:)) ;

first_param = [cam1(:) cam2(:)  cam3(:)];
second_param = [...
    [cam1(ligne_eau, colonne_eau); cam1(ligne_terre,colonne_terre)]...
    [cam2(ligne_eau, colonne_eau); cam2(ligne_terre,colonne_terre)]...
    [cam3(ligne_eau, colonne_eau); cam3(ligne_terre,colonne_terre)]...
    ];
    
