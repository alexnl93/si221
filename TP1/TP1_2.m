clear;

    X=[];
    Y=[];
    Z=[];
   
 % On peut remplacer 100 par un nombre plus grand
    
for i = 1:100
    R = 3 + 2.*randn(i,1);
    A = mean (R);
    B = var (R);
    
    X = [X i];
    Y = [Y A];
    Z = [Z B];
end

disp ('X:');
disp (X);

disp ('Y:');
disp (Y);

disp ('Z:');
disp (Z);

figure(1);
plot(X,Y,'c--*'); hold on;
plot(X,Z,'b--*'); hold on;
legend('moyenne','variance');

% Résultat: La moyenne de R tend vers 3 sur le graphique, et la variance
% tend vers 4. Autrement dit, le graphique coincide avec la définition de R