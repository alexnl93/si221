clear;

N = 100;
mu = [4 9];
S = mu'*ones(N,1)';

% Deuxième possibilité pour S:
% S = repmat(mu,N,1) 

X = S'+ rand(N,2);
A = mean (X); % A est la moyenne du vecteur 
B = var(X); % B est la variance du vecteur 

disp ('X:');
disp (X);

disp ('Le vecteur moyen est:');
disp (A);

disp ('Le vecteur variance est:');
disp (B);

% Conclusion: Le vecteur moyen, et le vecteur variance sont bien ceux
% attendus.
