clear;

mu1 = [4 9];
s1 = [2 2; 2 5];
P = rand(57,57);

if det(s1) == 0
    disp('impossible à inverser');
else
    s1i = inv(s1);
end

X = linspace(0.27, 12.5, 57);
Y = linspace(-2, 15, 57);
for i = 1:57
    for j = 1:57
        [P(i,j)] = dens(X(i),Y(j),mu1,s1,s1i);
    end
end

% On a crée la fonction densité en matlab à partir de la formule de la
% densité



