function [d] = dens(x,y,mu1,s,si)

a = exp((-1/2)*([x y]-mu1)*si*transpose([x y]-mu1));
b = (2*pi*sqrt(det(s)));

d = a/b;
end
