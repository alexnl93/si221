clear;

mu1 = [4 9];
s1 = [2 2; 2 5];

mu2 = [8.5 7.5];
s2 = [2 -2; -2 5];

mu3 = [6 3.5];
s3 = [7 -4; -4 7];

X = linspace(0.27, 12.5, 57);
Y = linspace(-2, 15, 57);

% La fonction mat_dens permet de calculé la densité des coordonées en
% fonction d'une classe. Le resultat ici est sous forme de matrice comme
% demandé

P1 = mat_dens(X,Y,mu1,s1);
P2 = mat_dens(X,Y,mu2,s2);
P3 = mat_dens(X,Y,mu3,s3);

figure(1);
contour(X,Y,P1,20,'ShowText','on');
figure(2);
contour(X,Y,P2,20);
figure(3);
contour(X,Y,P3,20);

figure(4);
mesh(X,Y,P1); hold on;
mesh(X,Y,P2); hold on;
mesh(X,Y,P3); hold on;

% Conclusion : Plus l'amplitude est grandes, plus on est sur d'avoir un élément
% appartenant à cette classe

