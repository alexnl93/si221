function [P] = mat_dens(X,Y,mu,s)

if det(s) == 0
    disp('impossible à inverser');
else
    si = inv(s);
end

P = rand(57,57);

for i = 1:57
    for j = 1:57
        [P(i,j)] = dens(X(i),Y(j),mu,s,si);
    end
end

end