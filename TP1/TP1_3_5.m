N = 100;

% generation de x1, x2 et x3
mu1 = [4 9];
s1 = [2 2; 2 5];
r1 = chol(s1);
X1 = repmat(mu1,N,1) + randn(N,2)*r1;

mu2 = [8.5 7.5];
s2 = [2 -2; -2 5];
r2 = chol(s2);
X2 = repmat(mu2,N,1) + randn(N,2)*r2;

mu3 = [6 3.5];
s3 = [7 -4; -4 7];
r3 = chol(s3);
X3 = repmat(mu3,N,1) + randn(N,2)*r3;

figure(6);
plot(X1(:,1),X1(:,2),'r*',X2(:,1),X2(:,2),'g +',X3(:,1),X3(:,2),'b o');
