clear;

N = 400;
mu = [4 9];
Sigma = [2 2; 2 5];
R = chol(Sigma);
X = repmat(mu,N,1) + randn(N,2)*R;
A = mean (X);
B = var(X);
C = cov(X);

%V vecteurs propres de sigmas
%S valeurs propres de Sigma
[V,S] = eigs(Sigma);
verif = V * S * transpose(V);

%Schéma avec les vecteurs directeurs
figure(1);
plot(X(:,1),X(:,2),'r*'); hold on;
plot([0,20*V(2,1)],[0,20*V(2,2)]);hold on;
plot([4,4+5*V(1,1)],[9,9+5*V(1,2)]);hold on;
legend('X','Vecteur base 1','Vecteur base 2');

disp ('Le vecteur moyen est:');
disp (A);

disp ('Le vecteur variance est:');
disp (B);

disp ('La matrice de covariance est:');
disp (C);

disp ('La matrice de valeur propre:');
disp (S);

disp ('La matrice de veteurs propre et de rotation:');
disp (V);

disp('Verification');
disp(verif);

% Conclusion : 
%   1) Le vecteur moyen, le vecteur variance et la matrice
% covariance sont bien ceux attendus.
%   2) La relation liant S à sa matrice digonalisé est bien vérifié