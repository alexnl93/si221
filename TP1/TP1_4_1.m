clear;

Z = [];

X = linspace(0.27, 12.5, 57);
Y = linspace(-2, 15, 57);

for i = 1:57
    for j = 1:57
        %les deux points sont utiliser pour remplir toutes les dimension
        Z(i,j,:) = [X(i),Y(j)];
    end
end

% Z est la grille demandée