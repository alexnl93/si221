clear;

N = 100;
mu = [4 9];
Sigma = [1 0; 0 6];
R = chol(Sigma);
S = mu'*ones(N,1)';

% Deuxième possibilité pour S:
% S = repmat(mu,N,1) 

X = S'+ rand(N,2);
A = mean (X);
B = var(X);
C = cov(X);

disp ('Le vecteur moyen est:');
disp (A);

disp ('Le vecteur variance est:');
disp (B);

disp ('La matrice de covariance est:');
disp (C);

% Conclusion : Le vecteur moyen, le vecteur variance et la matrice
% covariance sont bien ceux attendus.